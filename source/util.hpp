#ifndef __M_UTIL_H__
#define __M_UTIL_H__

#include"logger.hpp"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <memory>
#include <vector>
#include <cstdint>
#include <jsoncpp/json/json.h>
#include <mysql/mysql.h>

#include <websocketpp/server.hpp>
#include <websocketpp/config/asio_no_tls.hpp>

typedef websocketpp::server<websocketpp::config::asio> wsserver_t;

class  mysql_util
{
public:
    static MYSQL *mysql_create(const std::string &host,
        const std::string &username,
        const std::string &password,
        const std::string &dbname,
        uint16_t port = 3306)//数据库的也行访问端口或者0
        {
            MYSQL *mysql = mysql_init(NULL);
            //1.初始化句柄
            if (mysql == NULL) {
                ELOG("mysql init failed!");
                return NULL;
            }
            //2.设置字符集
            if (mysql_real_connect(mysql, 
                host.c_str(), 
                username.c_str(), 
                password.c_str(), 
                dbname.c_str(), port, NULL, 0) == NULL) {
                ELOG("connect mysql server failed : %s", mysql_error(mysql));
                mysql_close(mysql);
                return NULL;
            }
            //3.设置客户端字符集
             if (mysql_set_character_set(mysql, "utf8") != 0) {
                ELOG("set client character failed : %s", mysql_error(mysql));
                mysql_close(mysql);
                return NULL;
            }
            return mysql;
        }

        static bool mysql_exec(MYSQL *mysql, const std::string &sql)//mysql句柄和mysql语句
        {
            int ret = mysql_query(mysql, sql.c_str());//线程安全的。
                if (ret != 0) {
                    ELOG("%s\n", sql.c_str());
                    ELOG("mysql query failed : %s\n", mysql_error(mysql));
                    return false;
                }
                return true;
        }    

        static void mysql_destroy(MYSQL *mysql) 
        {
            if (mysql != NULL) {
                    mysql_close(mysql);
                }
                return ;
        }
    
};

class json_util{
    public:
    static bool serialize(const Json::Value &root, std::string &str)
    {
        //2. 实例化一个StreamWriterBuilder工厂类对象
        Json::StreamWriterBuilder swb;
        //3. 通过StreamWriterBuilder工厂类对象生产一个StreamWriter对象
        //std::unique_ptr<Json::StreamWriter>sw(swb.newStreamWriter());用智能指针就省去delete了，防止内存泄漏
        Json::StreamWriter *sw = swb.newStreamWriter();
        //4. 使用StreamWriter对象，对Json::Value中存储的数据进行  序列化
        std::stringstream ss;
        int ret = sw->write(root, &ss);

        if (ret != 0) {
            ELOG("json serialize failed!!\n");
            return false;
        }
        str=ss.str();
        delete sw;
        return true;
    }
    

    static bool unserialize(const std::string &str, Json::Value &root)
    {
        //1. 实例化一个CharReaderBuilder工厂类对象
        Json::CharReaderBuilder crb;

        //2. 使用CharReaderBuilder工厂类生产一个CharReader对象
        //Json::CharReader *cr = crb.newCharReader();//在堆new的，需要delete
        std::unique_ptr<Json::CharReader> cr(crb.newCharReader());//智能指针不需要delete
        //3. Json::Value对象root存储解析后的数据
        
        std::string err;
        //4. 使用CharReader对象进行json格式字符串str的反序列化
        // parse(char *start,  char *end,  Json::Value *val,  string *err);
        bool ret = cr->parse(str.c_str(), str.c_str() + str.size(), &root, &err);
        if (ret == false) {
            ELOG("json unserialize failed:%s ",err.c_str());
            return false;
        }
        return true;
        
    }
};

//string 工具类的实现
class string_util{
    public:
        static int split(const std::string &src/*表示整个字符串*/,const std::string &sep/*sep表示分隔符*/,std::vector<std::string>&res)//字符串分割截取子串
        {
            size_t pos,idx=0;
            while (idx<src.size())
            {
                pos=src.find(sep,idx);
                if(pos==std::string::npos)
                {
                    //没有找到分隔符
                    res.push_back(src.substr(idx));
                    break;
                }
                if(pos==idx)
                {
                    idx+=sep.size();
                    continue;
                }
                res.push_back(src.substr(idx,pos-idx));
                idx=pos+sep.size();
            }
            return res.size();
            
        }
};

class file_util {
   public:
        static bool read(const std::string &filename, std::string &body) {
            //打开文件
            std::ifstream ifs(filename, std::ios::binary);//打开的文件不一定文本，所以以源数据打开最好
            if (ifs.is_open() == false) {
                ELOG("%s file open failed!!", filename.c_str());
                return false;
            }
            //获取文件大小
            size_t fsize = 0;
            ifs.seekg(0, std::ios::end);//跳转到文件末尾
            fsize = ifs.tellg();//获取当前位置和读取位置的偏移量
            ifs.seekg(0, std::ios::beg);
            body.resize(fsize);//让body的大小扩大成fsize
            //将文件所有数据读取出来
            ifs.read(&body[0], fsize);
            if (ifs.good() == false) {
                ELOG("read %s file content failed!", filename.c_str());
                ifs.close();
                return false;
            }
            //关闭文件
            ifs.close();
            return true;
        }
};


#endif