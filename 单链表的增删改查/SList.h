#define _CRT_SECURE_NO_WARNINGS
#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLDataType;
typedef struct SListNode
{
	SLDataType data;
	struct SListNode* next;
}SLTNode;

//创建一个单链表的结点
SLTNode* BuySLTNode(SLDataType x);
//打印单链表
void SListPrint(SLTNode* phead);
//单链表的销毁
void SListDestroy(SLTNode** pphead);
//单链表头插
void SListPushFront(SLTNode** pphead, SLDataType x);
//单链表尾插
void SListPushBack(SLTNode** pphead, SLDataType x);
//单链表尾删
void SListPopBack(SLTNode** pphead);
//单链表头删
void SListPopFront(SLTNode** pphead);

//查找
SLTNode* SListFind(SLTNode* phead, SLDataType x);

//在pos之前插入
void SListInsert(SLTNode** pphead, SLTNode* pos, SLDataType x);

//删除pos位置的结点
void SListErase(SLTNode** pphead, SLTNode* pos);

//删除pos后面位置的结点
void SListEraseAfter(SLTNode* pos);

