#define _CRT_SECURE_NO_WARNINGS
#include"SList.h"
//创建一个新结点
SLTNode* BuySLTNode(SLDataType x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	if (newnode==NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	newnode->data = x;
	newnode->next = NULL;
	return newnode;
}

//打印单链表
void SListPrint(SLTNode* phead)
{
	SLTNode* tmp = phead;
	while (tmp != NULL)
	{
		printf("%d->", tmp->data);
		tmp = tmp->next;
	}
	printf("NULL\n");
}

//单链表的销毁
void SListDestroy(SLTNode** pphead)//这里就是为了能够改变头指针的值，针对除了头结点以外的结点，其实就可以不用二级指针。
{
	assert(pphead);
	SLTNode* tmp = *pphead;
	while (tmp)//两个指针next 和 tmp ，next负责存下一个结点的地址，tmp负责销毁当前结点
	{

		SLTNode* next = tmp->next;
		free(tmp);
		tmp = next;

	}
	*pphead = NULL;
}

//单链表头插/////////////////////////////////////////////////
void SListPushFront(SLTNode** pphead, SLDataType x)
{
	assert(pphead);
	SLTNode* tmp = *pphead;
	*pphead = BuySLTNode(x);
	(*pphead)->next = tmp;

	/*SLTNode* newnode = BuySLTNode(x);
	newnode->next = *pphead;
	*pphead = newnode;*/

}

//单链表尾插
void SListPushBack(SLTNode** pphead, SLDataType x)
{
	assert(pphead);
	SLTNode* tmp = *pphead;
	if (*pphead==NULL)
	{
		*pphead = BuySLTNode(x);
	}
	else
	{
		while (tmp->next)
		{
			tmp = tmp->next;
		}
		tmp->next = BuySLTNode(x);
	}

	//assert(pphead);

	//SLTNode* newnode = BuySLTNode(x);

	//// 1、空
	//// 2、非空
	//if (*pphead == NULL)
	//{
	//	*pphead = newnode;
	//}
	//else
	//{
	//	// 找尾
	//	SLTNode* tail = *pphead;
	//	while (tail->next != NULL)
	//	{
	//		tail = tail->next;
	//	}

	//	tail->next = newnode;
	//}
}

//单链表尾删
void SListPopBack(SLTNode** pphead)
{
	//没有结点，还要删除的情况
	if (*pphead==NULL)
	{
		return;
	}
	assert(*pphead != NULL);
	

	SLTNode* tmp = *pphead;
	if ((*pphead)->next==NULL)//一个结点删除的情况
	{
		free(*pphead);
		*pphead = NULL;
	}
	else//能走到这里，就是多个结点的情况了
	{
		while (tmp->next->next)
		{
			tmp = tmp->next;
		}
		free(tmp->next);
		tmp->next = NULL;
	}
	
	//assert(pphead);

	//// 温柔的检查
	//if (*pphead == NULL)
	//{
	//	return;
	//}

	//// 暴力检查
	////assert(*pphead != NULL);


	//// 1、一个节点
	//// 2、多个节点
	//if ((*pphead)->next == NULL)
	//{
	//	free(*pphead);
	//	*pphead = NULL;
	//}
	//else
	//{
	//	// 找尾
	//	/*SLTNode* prev = NULL;
	//	SLTNode* tail = *pphead;
	//	while (tail->next != NULL)
	//	{
	//	prev = tail;
	//	tail = tail->next;
	//	}

	//	prev->next = NULL;
	//	free(tail);
	//	tail = NULL;*/

	//	SLTNode* tail = *pphead;
	//	while (tail->next->next != NULL)
	//	{
	//		tail = tail->next;
	//	}

	//	free(tail->next);
	//	tail->next = NULL;
	//}
}

//单链表头删
void SListPopFront(SLTNode** pphead)
{
	assert(pphead);
	if (*pphead==NULL)
	{
		return;
	}

	SLTNode* tmp = *pphead;
	if ((*pphead)->next==NULL)
	{
		*pphead = NULL;
	}
	else
	{
		*pphead = tmp->next;
	}
	free(tmp);
	tmp = NULL;

	//assert(pphead);

	//// 温柔的检查
	//if (*pphead == NULL)
	//{
	//	return;
	//}

	//// 暴力检查
	////assert(*pphead != NULL);

	//SLTNode* del = *pphead;
	//*pphead = (*pphead)->next;
	//free(del);
	//del = NULL;
}

//查找
SLTNode* SListFind(SLTNode* phead, SLDataType x)
{

	SLTNode* tmp = phead;
	while (tmp)
	{
		if (tmp->data==x)
		{
			return tmp;
		}
		tmp = tmp->next;
	}
	return NULL;
}

//在pos之前插入
void SListInsert(SLTNode** pphead, SLTNode* pos, SLDataType x)
{
	assert(pphead);
	assert(pos);

	if (*pphead==pos)
	{
		//一个结点

		SLTNode* tmp = BuySLTNode(x);
		tmp->next = *pphead;
		*pphead = tmp;
	}
	else
	{
		//两个及以上结点
		SLTNode* before = *pphead;
		while (before->next != pos)
		{
			before = before->next;
			assert(before);//假如传的是一个错误的位置，它既不是NULL，也不是某个结点的位置
		}
		before->next = BuySLTNode(x);

		(BuySLTNode(x))->next = pos;
	}
	
	//	assert(pphead);
	//assert(pos);

	//if (pos == *pphead)
	//{
	//	SListPushFront(pphead, x);
	//}
	//else
	//{
	//	SLTNode* prev = *pphead;
	//	while (prev->next != pos)
	//	{
	//		prev = prev->next;

	//		// 暴力检查，pos不在链表中.prev为空，还没有找到pos，说明pos传错了
	//		assert(prev);
	//	}

	//	SLTNode* newnode = BuySLTNode(x);
	//	prev->next = newnode;
	//	newnode->next = pos;
	//}
	//

}

//删除pos位置的结点
void SListErase(SLTNode** pphead, SLTNode* pos)
{
	assert(pphead);
	assert(pos);
	if (*pphead==pos)//就一个结点的情况，直接就是头删
	{
		SListPopFront(pphead);
	}
	else//多个结点的情况，从前到后遍历找到pos前面的那个结点
	{
		SLTNode* before = *pphead;
		while (before->next != pos)
		{
			before = before->next;
		}
		
		before->next = pos->next;
		free(pos);
	}
	
}


//删除pos后面位置的结点
void SListEraseAfter(SLTNode* pos)
{
	assert(pos);
	if (pos->next==NULL)
	{
		return;
	}
	if (pos->next->next==NULL)
	{
		free(pos->next);
		pos->next == NULL;
	}
	else
	{
		SLTNode*tmp= pos->next->next;
		free(pos->next);
		pos->next = tmp;

	}

	/*assert(pos);

	if (pos->next == NULL)
	{
		return;
	}
	else
	{
		SLTNode* next = pos->next;
		pos->next = next->next;
		free(next);
	}*/
}