﻿#define _CRT_SECURE_NO_WARNINGS
#include "SList.h"

void TestSList4()
{
	SLTNode* plist = NULL;
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	SLTNode* pos = SListFind(plist, 3);
	if (pos)
	{
		SListErase(&plist, pos);
		pos = NULL;
	}
	SListPrint(plist);

	pos = SListFind(plist, 1);
	if (pos)
	{
		SListErase(&plist, pos);
		pos = NULL;
	}
	SListPrint(plist);
}

int main()
{
	TestSList4();

	return 0;
}
