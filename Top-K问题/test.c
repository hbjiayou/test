#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int HPDataType;

void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustDown(HPDataType* a, int n, int parent)
{
	int minChild = parent * 2 + 1;
	while (minChild < n)
	{
		// 找出小的那个孩子
		if (minChild + 1 < n && a[minChild + 1] < a[minChild])
		{
			minChild++;
		}

		if (a[minChild] < a[parent])
		{
			Swap(&a[minChild], &a[parent]);
			parent = minChild;
			minChild = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void PrintTopK(int* a, int k, int num)
{
	assert(a);

	// 建k个数的小堆
	for (int j = (k - 1 - 1) / 2; j >= 0; --j)
	{
		AdjustDown(a, k, j);
	}

	// 继续读取后N-K
	int tmp = k;
	while (tmp < num)
	{
		tmp++;
		if (a[tmp] > a[0])//遍历后面的元素，比堆顶大就交换
		{
			a[0] = a[tmp];
			AdjustDown(a, k, 0);
		}

	}

	for (int i = 0; i < k; ++i)
	{
		printf("%d ", a[i]);
	}
}

int main()
{
	int a[] = { 10,2,5,45,48,7,3,64,82,49,22,0,548,1001,1002,1003,1004 };
	int num = sizeof(a) / sizeof(int);
	PrintTopK(a, 4, num);
	return 0;
}