作业标题
字符串左旋

作业内容
实现一个函数，可以左旋字符串中的k个字符。


例如：


ABCD左旋一个字符得到BCDA

ABCD左旋两个字符得到CDAB

我的答案：
（暴力输出，只能用一次，字符串已经面目全非）
#include<stdio.h>
int main()
{
	char str[100] = { 0 };
	gets(str);
	int num = 0;
	scanf("%d", &num);
	printf("%s", str + num);
	str[num] = '\0';
	printf("%s", str);
	return 0;
}

老师的答案：
//实现一个函数，可以左旋字符串中的k个字符。
void left_rotate(char arr[], int k)
{
	int i = 0;
	int len = strlen(arr);
	k %= len;          //防止逆置的个数超过整个字符串的长度
	for (i = 0; i < k; i++)
	{
		//旋转1个字符
		//1
		char tmp = arr[0];
		//2
		int j = 0;
		for (j = 0; j < len - 1; j++)
		{
			arr[j] = arr[j + 1];
		}
		//3
		arr[len - 1] = tmp;
	}
}


int main()
{
	char arr[] = "abcdef";//cdefab
	int k = 0;
	scanf("%d", &k);//2
	left_rotate(arr, k);
	printf("%s\n", arr);

	return 0;
}

老师的另一个做法：
#include <assert.h>

void reverse(char* left, char* right)
{
	assert(left && right);
	while (left < right)
	{
		char tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}

void left_rotate(char arr[], int k)
{
	int len = strlen(arr);
	k%=len;//防止逆置的个数超过整个字符串的长度
	reverse(arr, arr+k-1);//左
	reverse(arr+k, arr+len-1);//右
	reverse(arr, arr + len-1);//整
}

原理：以逆置两个为例：
abcdef//把左面的ab逆置
bacdef//把右面的cdef逆置
bafedc//把整个字符串都逆置
cdefab

int main()
{
	char arr[] = "abcdef";//cdefab
	int k = 0;
	scanf("%d", &k);//2
	left_rotate(arr, k);
	printf("%s\n", arr);

	return 0;
}



