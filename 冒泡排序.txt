作业内容
写代码将三个整数数按从大到小输出。

例如：

输入：2 3 1

输出：3 2 1

第七题（仿冒泡排序）//#include<stdio.h>
//int main() {
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int hehe = 0;
//	scanf("%d %d %d",&a,&b,&c);
//	if (b>a)
//	{
//		hehe = a;
//		a = b;
//		b = hehe;
//	}
//	if (c>b) 
//	{
//		hehe = c;
//		c = b;
//		b = hehe;
//	}
//	if (c>a)
//	{
//		hehe = c;
//		c = a;
//		a = hehe;
//	}
//
//	if (b > a)
//	{
//		hehe = a;
//		a = b;
//		b = hehe;
//	}
//	printf("%d %d %d", a,b,c);
//    return 0;
//}

（真正的冒泡排序）//#include <stdio.h>
//int main() {
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	scanf("%d %d %d", &a, &b, &c);
//	int arr[] = { a, b, c };
//	int len = (int)sizeof(arr) / sizeof(*arr);
//	int i, j, temp;
//	for (i = 0; i < len - 1; i++)
//		for (j = 0; j < len - 1 - i; j++)
//			if (arr[j] < arr[j + 1]) {
//				temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//	int x;
//	for (x = 0; x < len; x++)
//		printf("%d ", arr[x]);
//	return 0;
//}

（这也是冒泡排序）//#include <stdio.h>//冒泡排序
//void bubble_sort(int arr[], int len) {
//	int i, j, temp;
//	for (i = 0; i < len - 1; i++)
//		for (j = 0; j < len - 1 - i; j++)
//			if (arr[j] > arr[j + 1]) {
//				temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//}
//int main() {
//	int arr[] = { 22, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70 };
//	int len = (int)sizeof(arr) / sizeof(*arr);
//	bubble_sort(arr, len);
//	int i;
//	for (i = 0; i < len; i++)
//		printf("%d ", arr[i]);
//	return 0;
//}


冒泡排序函数终极优化
void bubble_sort(int arr[], int sz)
{
	int i = 0;
	//趟数
	for (i = 0; i < sz - 1; i++)
	{
		int flag = 1;//假设数组是排好序
		//一趟冒泡排序的过程
		int j = 0;
		for (j = 0; j < sz-1-i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
				flag = 0;//说明数组还没排好序
			}
		}
		if (flag == 1)
		{
			break;
		}
	}
}
