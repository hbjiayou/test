#define _CRT_SECURE_NO_WARNINGS

/* srand example */
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

//int main()
//{
//	printf("First number: %d\n", rand() % 100);
//	srand(time(NULL));
//	printf("Random number: %d\n", rand() % 100);
//	srand(5);
//	printf("Again the first number: %d\n", rand() % 100);
//
//	return 0;
//}


int main() {
    int a, i;
    srand(time(NULL));//相当于一个起点，time随时间一直在变，下一次进入程序就又是一组新的随机数
    //使用for循环生成10个随机数
    for (i = 0; i < 10; i++) {
        
        a = rand();
        printf("%d ", a);
    }
    return 0;
}


//#include <stdlib.h>
//#include <stdio.h>
//
//int main(void)
//{
//    int i;
//
//    printf("Ten random numbers from 0 to 99\n\n");
//    for (i = 0; i < 10; i++)
//        printf("%d\n", rand() % 100);
//    return 0;
//}