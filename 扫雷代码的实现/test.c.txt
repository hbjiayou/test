#define _CRT_SECURE_NO_WARNINGS

#include"game.h"

void InitBoard(char board[ROWS][ROWS], int rows, int cols,char set)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
		{
			board[i][j]=set;
		}
	}
}


void Displayboard(char board[ROWS][COLS], int row, int col)//数组传参传的是首元素的地址
{
	int i = 0;
	int j = 0;
	printf("\n");
	printf("------扫雷游戏-------\n");
	for (j = 0; j <= col; j++)
	{
		printf("%d ", j);
	}
	printf("\n");

	for ( i = 1; i <=row; i++)
	{
		printf("%d ", i);
		for ( j = 1; j <=col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
	printf("------扫雷游戏-------\n");
	printf("\n");
}


void SetMine(char board[ROWS][COLS], int row, int col)
{
	int count = NumberMines;
	//这里x和y的值的范围为1~9,画图看一下
	while (count)
	{
		int x = rand() % 9 + 1;
		int y = rand() % 9 + 1;
		if (board[x][y]=='0')
		{
			board[x][y] = '1';
			count--;
		}
	}
}

int  get_mine_count(char board[ROWS][COLS], int x, int y)
{
	return board[x - 1][y - 1] +
		board[x - 1][y] +
		board[x - 1][y + 1] +
		board[x][y - 1] +
		board[x][y + 1] +
		board[x + 1][y - 1] +
		board[x + 1][y] +
		board[x + 1][y + 1] - 8 * '0';
}




void expand(char mine[ROWS][COLS],char show[ROWS][COLS], int x, int y)
{
	if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
	{


		for (int i = x - 1; i <= x + 1; i++)
		{
			for (int j = y - 1; j <= y + 1; j++)
			{
				/*if (show[i][j] == ' ')
				{

					break;
				}*/
				/*show[i][j] = ' ';*/
				if (mine[i][j]=='0')//它是雷，有可能他的周围没有雷，这时就不能进入循环，程序走到这里就要停下来
				{
					/*if (i != x && j != y && i + 1 <= ROW && j + 1 <= COL && i - 1 >= 1 && j - 1 >= 1)
					{*/

						int count = get_mine_count(mine, i, j);
						if (count == 0)
						{
							if (show[i][j] == '*')
							{
								show[i][j] = ' ';
								expand(mine, show, i, j);
							}
							/*expand(mine, show, i, j);*/
						}
						else
						{
							show[i][j] = count + '0';
						}
					//}
					/*else
					{
						break;
					}*/
				}
				
			}

		}
	}
}




int IsWin(char show[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	int count = 0;
	for (i = 1; i <= row; i++)
	{
		for (j = 1; j <= col; j++)
		{
			if (show[i][j] == '*')
			{
				count++;
			}
		}
	}
	return count == NumberMines;
}

void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)//字符'1'和数字1的区分、如何转化
{
	int x = 0;
	int y = 0;
	int win = 0;
	while (IsWin(show, row, col)!=1)//扫雷的规则就是把不是雷的地方点出来即可赢得比赛
	{
		printf("请输入要排查的坐标:>");
		scanf("%d%d", &x, &y);

		if (x>=1&&x<=row&&y>=1&&y<=col)
		{
			if (show[x][y]=='*')
			{
				//如果是雷
				if (mine[x][y] == '1')
				{
					printf("很遗憾，你被炸死了\n");
					Displayboard(mine, ROW, COL);
					break;
				}
				else//如果不是雷
				{
					//统计mine数组中x，y坐标周围有几个雷
				
					int count = get_mine_count(mine, x, y);

					if (count==0)
					{
						expand(mine, show,x, y);

					}
					else
					{
						show[x][y] = count + '0';//转换成数字字符
					}
					
					Displayboard(show, ROW, COL);
				}
			}
			else
			{
				printf("你输入的坐标已经被排过了，请重新输入\n");
			}
		}
		else
		{
			printf("输入的坐标非法，请重新输入\n");
		}
		
	}
	if (IsWin(show, row, col)==1)
	{
		printf("恭喜你， 排雷成功\n");
		//DisplayBoard(mine, ROW, COL);
	}
}