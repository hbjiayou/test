#define _CRT_SECURE_NO_WARNINGS

#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef int SLDataType;
typedef struct SeqList
{
	SLDataType* a;
	int size;//实际存储的个数
	int capacity;//当前存储容量
}SL;

//准备工作
void SLInit(SL* psl);
void SLDestory(SL* psl);
void SLPrint(SL* psl);

//头插头删  
void SLPushFront(SL* psl,SLDataType x);
void SLPopFront(SL* psl);
//尾插尾删
void SLPushBack(SL* psl,SLDataType x);
void SLPopBack(SL* psl);

//查找某个值，找到就返回下标，没找到就返回-1
int SLFind(SL* psl, SLDataType x);

//顺序表在pos位置插入x（位置就是下标）
void SLInsert(SL* psl, size_t pos, SLDataType x);

//顺序表删除pos位置的值
void SLErase(SL* psl, size_t pos);

//顺序表的修改
void SLModify(SL* psl, size_t pos, SLDataType x);