#define _CRT_SECURE_NO_WARNINGS

#include"SeqList.h"

//准备工作
void SLInit(SL* psl)
{
	assert(psl);

	psl->a = NULL;
	psl->size = psl->capacity = 0;
}
void SLDestory(SL* psl)
{
	assert(psl);

	free(psl->a);
	psl->a = NULL;
	psl->capacity = psl->size = 0;
}

void SLPrint(SL* psl)
{
	assert(psl);
	for (int i = 0; i < psl->size; i++)
	{
		printf("%d ", psl->a[i]);
	}
	printf("\n");
}

void SLCheckCapacity(SL* psl)///////////////////////////////虽然没在声明里，但是很重要
{
	//检查容量
	if (psl->capacity == psl->size)
	{
		int newCapacity = psl->capacity == 0 ? 4 : psl->capacity * 2;//判断是初始的0还是使用之后不够了才来的
		SLDataType* tmp = (SLDataType*)realloc(psl->a, newCapacity * sizeof(SLDataType));
		if (tmp == NULL)
		{
			perror("realloc fail");
			return;
		}

		psl->a = tmp;
		psl->capacity = newCapacity;
	}
}///////////////////////////////////////////////////////////

//头插头删
void SLPushFront(SL* psl, SLDataType x)
{
	assert(psl);
	SLCheckCapacity(psl);

	//移动数据
	int end = psl->size - 1;
	while (end)
	{
		psl->a[end] = psl->a[end - 1];
		end--;
	}
	psl->a[0] = x;
	psl->size++;
}

void SLPopFront(SL* psl)
{
	assert(psl);
	assert(psl->size > 0);

	int begin = 0;
	while (begin < psl->size - 1)
	{
		psl->a[begin] = psl->a[begin + 1];
		begin++;
	}
	psl->size--;

}

//尾插尾删
void SLPushBack(SL* psl, SLDataType x)
{
	assert(psl);
	SLCheckCapacity(psl);
	psl->a[psl->size] = x;
	psl->size++;
}

void SLPopBack(SL* psl)
{
	assert(psl);
	if (psl->size)//温柔的检查
	{
		return;
	}
	assert(psl->size > 0);
	psl->size--;
}

//查找
int SLFind(SL* psl, SLDataType x)
{
	assert(psl);
	for (int i = 0; i < psl->size; i++)
	{
		if (psl->a[i] == x)
		{
			return i;
		}
	}
	return -1;
}
//插入
void SLInsert(SL* psl, size_t pos, SLDataType x)
{
	assert(psl);
	assert(pos <= psl->size);
	SLCheckCapacity(psl);
	int tmp = psl->size - 1;
	int tmp1 = psl->size - pos;
	while (tmp1)
	{
		psl->a[tmp + 1] = psl->a[tmp];
		tmp1--;
	}
	psl->a[pos] = x;
	psl->size++;

	//老师的版本
	/*size_t end = psl->size;
	while (end > pos)
	{
		psl->a[end] = psl->a[end - 1];
		end--;
	}
	psl->a[pos] = x;
	psl->size++;*/

}
//删除
void SLErase(SL* psl, size_t pos)
{
	assert(psl);
	assert(pos < psl->size);
	size_t begin = pos;
	while (begin<psl->size-1)
	{
		psl->a[begin] = psl->a[begin + 1];
		begin++;
	}
	psl->size--;
}

//改
void SLModify(SL* psl, size_t pos, SLDataType x)
{
	assert(psl);
	assert(pos < psl->size);

	psl->a[pos] = x;
}