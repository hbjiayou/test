#define _CRT_SECURE_NO_WARNINGS
#include"Heap.h"


void HeapPrint(HP* php)
{
	for (int i = 0; i < php->size; ++i)
	{
		printf("%d ", php->a[i]);
	}
	printf("\n");
}

void HeapInit(HP* php)
{
	assert(php);
	php->a = NULL;
	php->size = php->capacity = 0;
}

void HeapDestroy(HP* php)
{
	assert(php);

	free(php->a);
	php->a = NULL;
	php->capacity = php->size = 0;
}

void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustUp(HPDataType* a, int child)//向上调整
{
	int parent = (child - 1) / 2;
	//while (parent >= 0)
	while (child > 0)
	{
		if (a[child] > a[parent])//跟祖先换，因为不论是大顶堆还是小顶堆祖先都是最大的那个或者最小的那个
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}


//插入x继续保持堆形态
void HeapPush(HP* php, HPDataType x)
{
	assert(php);
	if (php->size == php->capacity)
	{
		int newcapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(php->a, sizeof(HPDataType) * newcapacity);
		if (tmp==NULL)
		{
			perror("realloc fail");
			exit(-1);
		}
		else
		{
			php->a = tmp;
			php->capacity = newcapacity;
		}
	}
	php->a[php->size] = x;
	php->size++;

	AdjustUp(php->a, php->size - 1);//在a这个数组里，把下标为size-1这个元素向上调整
}

void AdjustDown(HPDataType * a, int n, int parent)//这里是以大顶堆向上调整的，若是小顶堆，要把下面的'>'换成'<'
{
	int minChild = parent * 2 + 1;//假设就把左孩子当作最小的那个
	while (minChild < n)//要小于当前数组总共的元素个数
	{
		// 找出大的那个孩子
		if (minChild + 1 < n && a[minChild + 1] > a[minChild])//minChild + 1 < n极端情况
			
		{
			minChild++;
		}

		if (a[minChild] > a[parent])//找大的然后往上替换
		{
			Swap(&a[minChild], &a[parent]);
			parent = minChild;
			minChild = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

// 删除堆顶元素 -- 找次大或者次小
// O(logN)
void HeapPop(HP* php)
{
	assert(php);
	assert(!HeapEmpty(php));
	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;

	AdjustDown(php->a, php->size, 0);
}

// 返回堆顶的元素
HPDataType HeapTop(HP* php)
{
	assert(php);
	assert(!HeapEmpty(php));

	return php->a[0];
}

bool HeapEmpty(HP* php)
{
	assert(php);
	return php->size == 0;
}

int HeapSize(HP* php)
{
	assert(php);

	return php->size;
}