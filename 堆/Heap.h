#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;//有效元素个数
	int capacity;
}HP;

//打印堆的元素
void HeapPrint(HP* php);
//交换函数
void Swap(HPDataType* p1, HPDataType* p2);
//向上调整
void AdjustUp(HPDataType* a, int child);
//向下调整
void AdjustDown(HPDataType* a, int n, int parent);

//初始化堆
void HeapInit(HP* php);
//销毁堆
void HeapDestory(HP* php);

//插入x继续保持堆形态
void HeapPush(HP* php, HPDataType x);

//删除堆顶元素--找次大或次小
void HeapPop(HP* php);

//堆顶元素
HPDataType HeapTop(HP* php);
//判断堆空
bool HeapEmpty(HP* php);
//堆的元素个数
int HeapSize(HP* php);